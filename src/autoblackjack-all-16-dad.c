/* Standard C libraries */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_GAMES 5800000

/* Local headers */
#include "blackjack.h"

/* Global variables */
//static const char *suit_names[NUMBER_SUITS] = {"Spade", "Heart", "Diamond", "Club"};


static const char *rank_names[NUMBER_RANKS] = {"Ace", "2", "3", "4", "5", "6", "7",
                                               "8", "9", "10", "Jack", "Queen", "King"};

#define frand() ((float)rand()/(float)RAND_MAX )

char virtual_dad( unsigned total ) {

	if (total <= 12) {
		return 'h';
	} else if (total == 13) {
		float r = frand();
		if (r < 0.99) return 'h';
		else return 's';
	} else if (total == 14) {
		float r = frand();
		if (r < 0.97) return 'h';
		else return 's';
	} else if (total == 15) {
		float r = frand();
		if (r < 0.82) return 'h';
		else return 's';
	} else if (total == 16) {
		float r = frand();
		if (r < 0.41) return 'h';
		else return 's';
	} else if (total == 17) {
		float r = frand();
		if (r < 0.01) return 'h';
		else return 's';
	} else {
		return 's';
	}
	
}

/* Shuffle a deck of cards */



static inline void shuffle(Card *cards)
{

    Card *aCard = cards;
    const Card *cardEnd = cards + NUMBER_CARDS;



    printf("Shuffling... ");

    /* Shuffle the cards */
    srand(time(NULL));
    aCard = cards;
    do
    {
        Card *swapPosition = cards + rand() % NUMBER_CARDS;
        Card swap = *aCard;
        *aCard = *swapPosition;
        *swapPosition = swap;
    }
    while (++aCard != cardEnd);

    printf("done\n");
}

/* Show a player or dealer's hand */
static inline void show_hand(char* tag, Card **hand, Card **end, uint points)
{
    Card **aCard = hand;

    printf("%s Cards: %s", tag, rank_names[(*aCard)->rank]);

    while (++aCard != end)
    {
        printf(" %s",
               rank_names[(*aCard)->rank]);
    }

    printf("\n");
}

int main(void)
{
    Card cards[NUMBER_CARDS];
    Card *aCard = cards;
    uint roundCount = NUMBER_ROUNDS_SHUFFLE, gamesPlayed = 0, gamesWon = 0;
    char buffer[80];

    /* Load the cards */
    uint i = 0;
    do
    {
        uint j = 0;
        do
        {
            aCard->suit = i;
            aCard->rank = j;
            ++aCard;
        }
        while (++j < NUMBER_RANKS);
    }
    while (++i < NUMBER_SUITS);

    /* Play */
    do {

        uint aces = 0, playerPoints = 0, dealerPoints = 0;
        Card *playerHand[MAXIMUM_HAND];
        Card **aHandCard = playerHand;

        /* Shuffle the cards after a specific number of rounds */
        if (++roundCount >= NUMBER_ROUNDS_SHUFFLE)
        {
            shuffle(cards);
            aCard = cards;
            roundCount = 0;
        }
printf("\nGame #%u of %u\n",gamesPlayed+1, MAX_GAMES);
        /* Player plays */
        do
        {
            /* Add the card's rank to player point */
            if (aCard->rank == ACE)
            {
                playerPoints += 11;
                ++aces;
            }
            else if (aCard->rank < JACK)
                playerPoints += aCard->rank + 1;
            else
                playerPoints += 10;

            /* Print the new card */
            //printf("New Card... %s \n", rank_names[aCard->rank]);

            *aHandCard = aCard;
            ++aCard;
            assert(aCard != cards + NUMBER_CARDS);

            /* Let the second card be dealt */
            if (aHandCard == playerHand)
            {
                ++aHandCard;
                *buffer = 'h';
                continue;
            }

            ++aHandCard;

            /* Convert Aces to 1 */
            while (playerPoints > 21 && aces > 0)
            {
                playerPoints -= 10;
                --aces;
            }

            /* Print the hand */
            show_hand("Player:", playerHand, aHandCard, playerPoints);

            /* On a bust, dealer doesn't need to play.
             * This single "goto" simplifies the code.
             */
            if (playerPoints > 21)
            {
                printf("Bust!\n");
                goto done;
            }

            /* On Blackjack, let the dealer play */
            if (playerPoints == 21)
            {
                printf("Blackjack!\n");
                break;
            }

            /*Hit or stay? */
            do
            {
                printf("Player: Hand Total %u | Hit or stay? (h/s) : ", playerPoints);
//                scanf("%s", buffer);
		sprintf(buffer,"%c",virtual_dad(playerPoints));
            }
            while (*buffer != 'h' && *buffer != 'H' && *buffer != 's' && *buffer != 'S');
        }
        while (*buffer == 'h' || *buffer == 'H');

        /* Dealer plays */
        aHandCard = playerHand;
        aces = 0;
        printf("Dealer plays...\n");
        do
        {
            /* Add the card's rank to dealer point */
            if (aCard->rank == ACE)
            {
                dealerPoints += 11;
                ++aces;
            }
            else if (aCard->rank < JACK)
                dealerPoints += aCard->rank + 1;
            else
                dealerPoints += 10;

            /* Print the new card */
            //printf("New Card... %s \n", rank_names[aCard->rank]);

            *aHandCard = aCard;
            ++aCard;
            assert(aCard != cards + NUMBER_CARDS);

            /* Let the second card be dealt */
            if (aHandCard == playerHand)
            {
                ++aHandCard;
                continue;
            }

            ++aHandCard;

            /* Convert Aces to 1 */
            while (dealerPoints > 21 && aces > 0)
            {
                dealerPoints -= 10;
                --aces;
            }

            /* Print the hand */
            show_hand("Dealer:", playerHand, aHandCard, dealerPoints);

            if (dealerPoints > 21)
            {
                printf("Bust!\n");
                break;
            }

            /* Blackjack */
            if (dealerPoints == 21)
            {
                printf("Blackjack!\n");
                break;
            }

            printf("Dealer: Hand Total %u | Hit or stay? (h/s) : ", dealerPoints);

            /* Dealer must hit on soft 17 */
            if (dealerPoints < 16 || (dealerPoints == 16 && aces))
            {
                printf("hit\n");
                continue;
            }

            printf("stay\n");
            break;
        }
        while (1);
done:

        /* Keep track of win percentage for the player. */
        if (playerPoints <= 21 && (playerPoints > dealerPoints || dealerPoints > 21))
        {
            ++gamesWon;
            printf("Player wins!\n");
        }
        else if (playerPoints <= 21 && playerPoints == dealerPoints)
            printf("Tie!\n");
        else
            printf("Dealer wins!\n");

        ++gamesPlayed;

        printf("Win percentage - %u%%\n", (gamesWon * 100) / gamesPlayed);


        /* Ask to play again. */
        //do {
          //  printf("Play again? (y/n) : ");
            //scanf("%s", buffer);
        //} while (*buffer != 'n' && *buffer != 'N' && *buffer != 'y' && *buffer != 'Y');
printf("\n");
    //} while (*buffer == 'y' || *buffer == 'Y');
    } while (gamesPlayed<MAX_GAMES);
    return 0;
}


