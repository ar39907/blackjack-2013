# Blackjack (2013)



## Description

Archived blackjack project from 2013.


## Getting Started

### Dependencies

* C Standard Library, GCC
* Linux


### Usage

* Compile blackjack.c
* Run executable


## Author

Austin Richie

https://gitlab.com/ar39907/
